// Copyright (C) 2006-2011 Kristian Oelgaard and Garth N. Wells.
// Licensed under the GNU LGPL Version 3.
//
// First added:  2006-11-13
// Last changed: 2011-02-06

#include <dolfin.h>
#include <FenicsSolidMechanics.h>

// Switch between linear and quadratic elements
#include "../forms/p2_forms/Plas3D.h"
//#include "../forms/p1_forms/Plas3D.h"

using namespace dolfin;

// Displacement right end
class DBval : public Expression
{
  public:

    DBval(const double& t) : t(t) {}

    void eval(Array<double>& values, const Array<double>& x) const
    {
      // Stretch
      values[0] = t*x[2];
      //values[0] = t;
    }
    const double& t;
};

// Sub domain for Dirichlet boundary condition
class DirichletBoundaryX1 : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  { return std::abs(x[0] - 1.0) < DOLFIN_EPS; }
};

// Sub domain for Dirichlet boundary condition
class DirichletBoundaryX0 : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  { return x[0] < DOLFIN_EPS; }
};

int main()
{
  Timer timer("Total plasicity solver time");

  //dolfin::parameters["reorder_dofs_serial"] = false;
  //dolfin::parameters["dof_ordering_library"] = "SCOTCH";
  //dolfin::parameters["linear_algebra_backend"] = "Epetra";

  // Create mesh
  UnitCubeMesh mesh(8, 8, 8);

  // Young's modulus and Poisson's ratio
  double E = 20000.0;
  double nu = 0.3;

  // Time parameter
  double t = 0.0;

  // Elastic time step, always one step.
  double Edt  = 0.0095;

  // Load region 0, time step and number of steps
  double dt0 = 0.001;
  unsigned int dt0_steps = 3;

  // Load region 1, time step and number of steps
  double dt1 = -0.002;
  unsigned int dt1_steps =  1;

  // Load region 2, time step and number of steps
  double dt2 = 0.001;
  unsigned int dt2_steps =  4;

  // Source term, RHS
  Constant f(0.0, 0.0, 0.0);

  // Function spaces
  Plas3D::FunctionSpace V(mesh);
  dolfin::cout << "Number of dofs: " << V.dim() << dolfin::endl;

  // Extract elements for stress and tangent
  std::shared_ptr<const FiniteElement> element_t;
  std::shared_ptr<const FiniteElement> element_s;
  {
    Plas3D::BilinearForm::CoefficientSpace_t Vt(mesh);
    element_t = Vt.element();
  }

  Plas3D::LinearForm::CoefficientSpace_s Vs(mesh);
  element_s = Vs.element();

  // Create boundary conditions (use SubSpace to apply simply
  // supported BCs)
  Constant zero(0.0, 0.0, 0.0);
  DBval val(t);
  SubSpace Vx(V, 0);

  DirichletBoundaryX0 dbX0;
  DirichletBoundaryX1 dbX1;
  DirichletBC bcX0(V, zero, dbX0);
  DirichletBC bcX1(Vx, val, dbX1);

  std::vector<const DirichletBC*> bcs;
  bcs.push_back(&bcX0);
  bcs.push_back(&bcX1);

  // Slope of hardening (linear) and hardening parameter
  const double E_t = 0.1*E;
  const double hardening_parameter = E_t/(1.0 - E_t/E);

  // Yield stress
  const double yield_stress = 200.0;

  // Solution function
  Function u(V);

  // Object of class von Mises
  const fenicssolid::VonMises J2(E, nu, yield_stress, hardening_parameter);

  // Constituive update
  std::shared_ptr<fenicssolid::ConstitutiveUpdate>
    constitutive_update(new fenicssolid::ConstitutiveUpdate(u, *element_s,
                                                            *Vs.dofmap(), J2));

  // Create forms and attach functions
  fenicssolid::QuadratureFunction tangent(mesh, element_t,
                                          constitutive_update,
                                          constitutive_update->w_tangent());

  Plas3D::BilinearForm a(V, V);
  a.t = tangent;

  Plas3D::LinearForm L(V);
  L.f = f;
  fenicssolid::QuadratureFunction stress(mesh, element_s,
                                         constitutive_update->w_stress());
  L.s = stress;

  // Create PlasticityProblem
  fenicssolid::PlasticityProblem nonlinear_problem(a, L, u, tangent,
                                                   stress, bcs, J2);

  // Create nonlinear solver and set parameters
  dolfin::NewtonSolver nonlinear_solver;
  nonlinear_solver.parameters["convergence_criterion"] = "incremental";
  nonlinear_solver.parameters["maximum_iterations"]    = 50;
  nonlinear_solver.parameters["relative_tolerance"]    = 1.0e-6;
  nonlinear_solver.parameters["absolute_tolerance"]    = 1.0e-15;

  // File names for output
  File file1("output/disp.pvd");
  File file2("output/eq_plas_strain.pvd");

  // Equivalent plastic strain for visualisation
  CellFunction<double> eps_eq(mesh);

  // Load-disp info
  unsigned int step = 0;
  unsigned int steps = dt0_steps + dt1_steps + dt2_steps + 1;
  while (step < steps)
  {
    // Use elastic tangent for first time step
    if (step == 0)
      t += Edt;
    else if (step < 1 + dt0_steps)
      t += dt0;
    else if (step < 1 + dt0_steps + dt1_steps)
      t += dt1;
    else if (step < 1 + dt0_steps + dt1_steps + dt2_steps)
      t += dt2;

    step++;
    std::cout << "step begin: " << step << std::endl;
    std::cout << "time: " << t << std::endl;

    // Solve non-linear problem
    nonlinear_solver.solve(nonlinear_problem, *u.vector());

    // Update variables
    //nonlinear_problem.update_history();
    constitutive_update->update_history();

    // Write output to files
    file1 << u;
    constitutive_update->eps_p_eq().compute_mean(eps_eq);
    file2 << eps_eq;
  }
  cout << "Solution norm: " << u.vector()->norm("l2") << endl;

  timer.stop();
  dolfin::list_timings();

  return 0;
}

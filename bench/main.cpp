
#include <dolfin.h>
#include <FenicsSolidMechanics.h>
#include <armadillo>
#include "Elasticity.h"
#include "Strain.h"

using namespace dolfin;

void disp(arma::vec vec)
{
  for (unsigned int i=0; i<vec.n_elem; i++)
    std::cout << vec[i] << ", ";
  std::cout << std::endl;
}

void disp(Array<unsigned int> vec)
{
  for (unsigned int i=0; i<vec.size(); i++)
    std::cout << vec[i] << ", ";
  std::cout << std::endl;
}

void disp(Array<double> vec)
{
  for (unsigned int i=0; i<vec.size(); i++)
    std::cout << vec[i] << ", ";
  std::cout << std::endl;
}

int main()
{
  // Sub domain for clamp at left end
  class Left : public SubDomain
  {
    bool inside(const Array<double>& x, bool on_boundary) const
    {
      return x[0] < DOLFIN_EPS;
    }
  };

  // Read mesh and create function space
  UnitCube mesh(8,8,8);
//  UnitCube mesh(1,1,1);
//  UnitSquare mesh(3,3);
  Elasticity::FunctionSpace V(mesh);
  Strain::FunctionSpace Vs(mesh);

  // Create right-hand side
  Constant f(0.7, -2.3, 1.6);
//  Constant f(0.7, -2.3);

  // Set up boundary condition at left end
  Constant zero(0.0, 0.0, 0.0);
//  Constant zero(0.0,0.0);
  Left left;
  DirichletBC bc(V, zero, left);

  // Set elasticity parameters
  double E  = 10.0;
  double nu = 0.3;
  Constant mu(E / (2*(1 + nu)));
  Constant lambda(E*nu / ((1 + nu)*(1 - 2*nu)));

  // Set up PDE (symmetric)
  Elasticity::BilinearForm a(V, V);
  a.mu = mu; a.lmbda = lambda;
  Elasticity::LinearForm L(V);
  L.f = f;

  LinearVariationalProblem problem(a, L, bc);
  problem.parameters["symmetric"] = true;

  // Solve PDE (using direct solver)
  Function u(V);
  problem.parameters("solver")["linear_solver"] = "direct";
  Timer timer_u("Compute displacements");
  problem.solve(u);
  timer_u.stop();

  Function eps(Vs);
  // Project strains.
  Timer timer_p("Compute strains (projection)");
  Strain::BilinearForm as(Vs, Vs);
  Strain::LinearForm Ls(Vs);
  Ls.U = u;
  VariationalProblem problem_s(as, Ls);
  problem_s.solve(eps);
  timer_p.stop();

  fenicsplas::StrainData sd(u, Vs[0]);

  FiniteElement q_elem = Vs[0]->element();
  const GenericDofMap* q_dofmap = &Vs[0]->dofmap();
  unsigned int num_ip = q_elem.space_dimension();
  Array<unsigned int> cell_dofs(Vs.element().space_dimension());
  Array<double> cell_vals(cell_dofs.size());

  unsigned int num_ip_vals = Vs.element().value_dimension(0);
  arma::vec ip_vals(num_ip_vals);

  // Loop over cells
  std::cout.precision(12);
  for (dolfin::CellIterator cell(mesh); !cell.end(); ++cell)
  {
    sd.update(*cell);
    Timer timer_l("Loop cells and extract computed strains");
    // Get projected strains
    Vs.dofmap().tabulate_dofs(cell_dofs.data().get(), *cell);
    eps.vector().gather(cell_vals, cell_dofs);
    timer_l.stop();

    for (unsigned int ip=0; ip<num_ip; ip++)
    {
      timer_l.start();
      for (unsigned int i=0; i<num_ip_vals; i++)
      {
        ip_vals[i] = cell_vals[i*num_ip + ip];
      }
      timer_l.stop();
      sd.strain(ip);
      // Test values
//      disp(ip_vals);
//      disp(sd.strain(ip));
//      arma::vec err = ip_vals - sd.strain(ip);
////      info("norm: %f", arma::norm(err, 2));
////      info("IP: %d, norm: %.12f", ip, arma::norm(err, 2));
//      assert(arma::norm(err, 2) < 10e-11);
    }
  }
  summary();
//  File file_eps("eps.xml");
//  file_eps << eps.vector();
//  // Plot solution
////  plot(u, "Displacement", "displacement");
//  File file_u("u.xml");
//  file_u << u.vector();
  return 0;
}

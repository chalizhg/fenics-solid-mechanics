// Copyright (C) 2013 Kristian Oelgaard and Garth N. Wells.
// Licensed under the GNU LGPL Version 3.
//
// First added:  2013-12-06


// This program tests the elastic and plastic load-displacement
// responses for a unit cube in uniaxial tension with an Von Mises
// (J2) model with linear strain hardening. The program will throw an
// error is any problems are detected. It uses P2 elements.

#include <dolfin.h>
#include <FenicsSolidMechanics.h>
#include "Plas3D.h"

using namespace dolfin;

// Displacement right end
class Load : public Expression
{
public:
  Load(const double& t) : Expression(3), t(t) {}
  void eval(Array<double>& values, const Array<double>& x) const
  { values[0] = 1.0e5*t; values[1] = 0.0; values[2] = 0.0; }
  const double& t;
};

// Right boundary (x=1)
class Right : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  { return std::abs(x[0] - 1.0) < DOLFIN_EPS; }
};

// Left boundary (x=1)
class Left : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  { return x[0] < DOLFIN_EPS; }
};

// Point x = (0, 0, 0)
class CornerPoint : public SubDomain
{
  bool inside(const Array<double>& x, bool on_boundary) const
  { return (x[0] < DOLFIN_EPS) && (x[1] < DOLFIN_EPS) && (x[2] < DOLFIN_EPS); }
};


int main()
{
  // Create mesh
  UnitCubeMesh mesh(8, 8, 8);

  // Young's modulus and Poisson's ratio
  double E = 20000.0;
  double nu = 0.3;

  // Slope of plastic-stress strain response and hardening parameter
  const double E_t = 0.1*E;
  const double hardening_parameter = E_t/(1.0 - E_t/E);

  // Yield stress
  const double yield_stress = 200.0;

  // Time parameter
  double t = 0.0;

  // Source term, RHS
  Constant f(0.0, 0.0, 0.0);

  // Function spaces
  Plas3D::FunctionSpace V(mesh);

  // Extract elements for stress and tangent
  boost::shared_ptr<const FiniteElement> element_t;
  boost::shared_ptr<const FiniteElement> element_s;
  {
    Plas3D::BilinearForm::CoefficientSpace_t Vt(mesh);
    element_t = Vt.element();
  }

  Plas3D::LinearForm::CoefficientSpace_s Vs(mesh);
  element_s = Vs.element();

  // Create boundary conditions (use SubSpace to apply simply
  // supported BCs)
  Constant zero(0.0);
  Constant zero_vector(0.0, 0.0, 0.0);
  Load boundary_load(t);
  SubSpace Vx(V, 0);

  Left left;
  CornerPoint corner;
  DirichletBC left_bc(Vx, zero, left);
  DirichletBC corner_bc(V, zero_vector, corner, "pointwise");

  std::vector<const DirichletBC*> bcs;
  bcs.push_back(&left_bc);
  bcs.push_back(&corner_bc);

  // Mark loading boundary
  Right right;
  FacetFunction<std::size_t> load_marker(mesh, 0);
  right.mark(load_marker, 1);

  // Solution function
  Function u(V);

  // Object of class von Mises
  const fenicssolid::VonMises J2(E, nu, yield_stress, hardening_parameter);

  // Constituive update
  boost::shared_ptr<fenicssolid::ConstitutiveUpdate>
    constitutive_update(new fenicssolid::ConstitutiveUpdate(u, *element_s,
                                                            *Vs.dofmap(), J2));

  // Create forms and attach functions
  fenicssolid::QuadratureFunction tangent(mesh, element_t,
                                          constitutive_update,
                                          constitutive_update->w_tangent());

  Plas3D::BilinearForm a(V, V);
  a.t = tangent;
  a.ds = load_marker;

  Plas3D::LinearForm L(V);
  L.f = f;
  L.h = boundary_load;
  fenicssolid::QuadratureFunction stress(mesh, element_s,
                                         constitutive_update->w_stress());
  L.s = stress;

  // Create PlasticityProblem
  fenicssolid::PlasticityProblem nonlinear_problem(a, L, u, tangent,
                                                   stress, bcs, J2);

  // Displacement and load integration functionals
  Plas3D::Form_M_d M_d(mesh, u);
  Plas3D::Form_M_f M_f(mesh, boundary_load);
  M_d.ds = load_marker;
  M_f.ds = load_marker;

  // Create nonlinear solver and set parameters
  dolfin::NewtonSolver nonlinear_solver;
  nonlinear_solver.parameters["convergence_criterion"] = "incremental";
  nonlinear_solver.parameters["maximum_iterations"]    = 50;
  nonlinear_solver.parameters["relative_tolerance"]    = 1.0e-6;
  nonlinear_solver.parameters["absolute_tolerance"]    = 1.0e-15;

  // Structures to hold load-disp data
  std::vector<double> disp, load;
  disp.push_back(0.0);
  load.push_back(0.0);

  // Solver loop
  unsigned int step = 0;
  unsigned int steps = 10;
  double dt = 0.001;
  while (step < steps)
  {
    t += dt;

    step++;
    std::cout << "step begin: " << step << std::endl;
    std::cout << "time: " << t << std::endl;

    // Solve non-linear problem
    nonlinear_solver.solve(nonlinear_problem, *u.vector());

    // Get displacement and applied force
    const double u_avg = assemble(M_d);
    disp.push_back(u_avg);
    const double force = assemble(M_f);
    load.push_back(force);

    // Check that slope is elastic before yield
    const std::size_t n = load.size();
    const double dload = load[n-1]- load[n-2];
    const double du    = disp[n-1] -disp[n-2];
    const double dE = std::abs(dload/du - E);
    if ((dE/E > 1.0e-10) && (load[n-1] < yield_stress))
      dolfin:error("Problem with yield point");

    // Update variables
    constitutive_update->update_history();
  }
  cout << "Solution norm: " << u.vector()->norm("l2") << endl;

  // Test elastic slope
  const double init_slope = (load[1] - load[0])/(disp[1] - disp[0]);
  std::cout << "Initial slope: " << init_slope << std::endl;
  const double dE = std::abs(init_slope - E);
  std::cout << "Initial slope diff: " << dE << std::endl;
  if (dE/E > 1.0e-10)
  {
    std::cout << "Initial load-displacement slope: " << init_slope << std::endl;
    std::cout << "Youngs modulus:                  " << E  << std::endl;
    std::cout << "Difference:                      " << dE << std::endl;
    dolfin::error("Elastic tangent does not match input Youngs's modulus");
  }

  // Test plastic slope
  const std::size_t num_steps = load.size();
  const double dload = load[num_steps-1] - load[num_steps-3];
  const double du = disp[num_steps-1] - disp[num_steps-3];
  const double plastic_slope = dload/du;
  std::cout << "Plastic slope: " << plastic_slope << std::endl;

  const double dEp = std::abs(plastic_slope - E_t);
  std::cout << "Plastic slope diff: " << dEp << std::endl;
  if (dEp/E_t > 1.0e-10)
  {
    std::cout << "Plastic load-displacement slope: " << plastic_slope
              << std::endl;
    std::cout << "Input plastic modulus:           " << E_t  << std::endl;
    std::cout << "Difference:                      " << dEp << std::endl;
    dolfin::error("Plastic tangent does not match input plastic modulus");
  }

  std::cout << "*** Test passed ***" << std::endl;

  return 0;
}

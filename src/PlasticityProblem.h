// Copyright (C) 2006-2011 Kristian Oelgaard and Garth N. Wells.
// Licensed under the GNU LGPL Version 3.
//
// First added:  2006-11-13
// Last changed: 2011-02-06

#ifndef __PLASTICITY_PROBLEM_H
#define __PLASTICITY_PROBLEM_H

#include <memory>
#include <vector>
#include <dolfin/nls/NonlinearProblem.h>
#include <dolfin/parameter/Parameters.h>

#include "HistoryData.h"
#include "PlasticityModel.h"
#include "ReturnMapping.h"

namespace dolfin
{
  class DirichletBC;
  class GenericDofMap;
  class GenericMatrix;
  class GenericVector;
  class Form;
  class Function;
  class NewtonSolver;
}

namespace fenicssolid
{

  class QuadratureFunction;

  class PlasticityProblem : public dolfin::NonlinearProblem
  {
  public:

    /// Constructor
    PlasticityProblem(const dolfin::Form& a, const dolfin::Form& L,
                      dolfin::Function& U,
                      QuadratureFunction& tangent,
                      QuadratureFunction& sigma,
                      const std::vector<const dolfin::DirichletBC*>& bcs,
                      const PlasticityModel& plastic_model);

    /// Constructor
    PlasticityProblem(const dolfin::Form& a, const dolfin::Form& L,
                      dolfin::Function& U,
                      QuadratureFunction& tangent,
                      QuadratureFunction& sigma,
                      const std::vector<const dolfin::DirichletBC*>& bcs,
                      const PlasticityModel& plastic_model,
                      std::shared_ptr<dolfin::NewtonSolver> newton_solver);

    /// Destructor
    ~PlasticityProblem();

    /// Loop quadrature points and compute local tangents and stresses
    void form(dolfin::GenericMatrix& A, dolfin::GenericVector& b,
              const dolfin::GenericVector& x);

    /// User defined assemble of residual vector
    void F(dolfin::GenericVector& b, const dolfin::GenericVector& x);

    /// User defined assemble of Jacobian matrix
    void J(dolfin::GenericMatrix& A, const dolfin::GenericVector& x);

    /// Update history variables
    void update_history();

    /// Public data members that might be of interest after the Newton
    /// solver has converged
    HistoryData eps_p_data;
    HistoryData eps_p_eq_data;

    /// Parameters
    dolfin::Parameters parameters;

  private:

    // For system after constitutive update
    void form_tensors(dolfin::GenericMatrix& A, dolfin::GenericVector& b,
                      const dolfin::GenericVector& x);

    // Assembler
    dolfin::SystemAssembler assembler;

    // Plasticity model
    const PlasticityModel& plastic_model;

    // Object to handle return mapping of stresses back to the yield
    // surface
    ReturnMapping return_mapping;

    // Helper data structures to handle variable updates on
    // integration points
    QuadratureFunction& _tangent;
    QuadratureFunction& _sigma;

    // Displacement
    const dolfin::Function& _u;

    // The Newton solver
    std::shared_ptr<dolfin::NewtonSolver> _newton_solver;

  };
}

#endif

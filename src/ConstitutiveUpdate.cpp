// Copyright (C) 2006-2010 Kristian Oelgaard and Garth N. Wells.
// Licensed under the GNU LGPL Version 3.
//
// First added:  2006-11-13
// Last changed: 2010-01-03

#include <vector>
#include <ufc.h>
#include <dolfin/fem/FiniteElement.h>
#include <dolfin/fem/GenericDofMap.h>
#include <dolfin/function/Function.h>
#include <dolfin/function/FunctionSpace.h>
#include <dolfin/la/GenericVector.h>
#include <dolfin/mesh/Cell.h>
#include "PlasticityModel.h"
#include "utils.h"
#include "ConstitutiveUpdate.h"

using namespace fenicssolid;

//-----------------------------------------------------------------------------
ConstitutiveUpdate::ConstitutiveUpdate(
  const dolfin::Function& u,
  const dolfin::FiniteElement& sigma_element,
  const dolfin::GenericDofMap& stress_dofmap,
  const PlasticityModel& plastic_model)
  : _gdim(u.function_space()->mesh()->geometry().dim()),
    _u(u),
    _plastic_model(plastic_model),
    _eps_p(*u.function_space()->mesh(), sigma_element, 6),
    _eps_p_equiv(*u.function_space()->mesh(), sigma_element, 1),
    _expansion_coeffs(u.function_space()->dofmap()->max_cell_dimension()),
    De(_plastic_model.elastic_tangent)
{
  // Compute shape derivatives on reference element
  const dolfin::FiniteElement& u_element = *_u[0].function_space()->element();
  boost::multi_array<double, 2> ip_coordinates;
  dolfin::CellIterator cell(*u.function_space()->mesh());
  std::vector<double> vertex_coordinates;
  cell->get_vertex_coordinates(vertex_coordinates);
  stress_dofmap.tabulate_coordinates(ip_coordinates, vertex_coordinates, *cell);

  //_derivs_reference
  //  = compute_basis_derivatives1(element, ip_coordinates, ufc_cell);
  compute_basis_derivatives1(u_element, ip_coordinates,
                             vertex_coordinates.data(), _derivs_reference);

  // Compute number of quadrature points per cell
  const std::size_t num_ip_dofs = sigma_element.value_dimension(0);
  num_ip_per_cell = sigma_element.space_dimension()/num_ip_dofs;

  // Resize _w_stress/tangent
  const::std::size_t sigma_element_dim = sigma_element.space_dimension();
  _w_stress.resize(num_ip_dofs*num_ip_per_cell);
  _w_tangent.resize(num_ip_dofs*num_ip_dofs*num_ip_per_cell);

  const std::size_t num_cells = u.function_space()->mesh()->num_cells();
  _plastic_last.resize(boost::extents[num_cells][num_ip_per_cell]);
  std::fill(_plastic_last.data(),
            _plastic_last.data() + _plastic_last.num_elements(),
            false);
}
//-----------------------------------------------------------------------------
ConstitutiveUpdate::~ConstitutiveUpdate()
{
  // Do nothing
}
//-----------------------------------------------------------------------------
void ConstitutiveUpdate::update(const dolfin::Cell& cell,
                                const double* vertex_coordinates)
{
  const std::size_t cell_index = cell.index();

  // Get solution dofs on cell
  const std::vector<dolfin::la_index>&
    dofs = _u.function_space()->dofmap()->cell_dofs(cell_index);

  // Get expansion coefficients on cell
  dolfin_assert(_expansion_coeffs.size()
                ==  _u.function_space()->dofmap()->max_cell_dimension());
  _u.vector()->get_local(_expansion_coeffs.data(), dofs.size(), dofs.data());

  Eigen::Matrix<double, 6, 6> cons_tangent;
  Eigen::Matrix<double, 6, 1> strain, strain_p, trial_stress;
  Eigen::Matrix<double, 1, 1> strain_p_eq;

  // Loop over quadrature points
  for (std::size_t ip = 0; ip < num_ip_per_cell; ip++)
  {
    // Assume elastic tangent
    cons_tangent = De;

    // Get plastic strain from previous converged time step
    _eps_p.get_old_values(cell_index, ip, strain_p);

    compute_strain_reference(strain, _derivs_reference[ip], vertex_coordinates,
                             _expansion_coeffs);

    // Compute trial stress
    trial_stress = De*(strain - strain_p);

    // Get equivalent plastic strain from previous converged time
    // step
    _eps_p_equiv.get_old_values(cell_index, ip, strain_p_eq);

    // Testing trial stresses, if yielding occurs the stresses are
    // mapped back onto the yield surface, and the updated
    // parameters are returned.
    const bool active = _plastic_last[cell_index][ip];
    return_mapping.closest_point_projection(_plastic_model, cons_tangent,
                                            trial_stress, strain_p,
                                            strain_p_eq(0),
                                            active);
    _plastic_last[cell_index][ip] = false;

    // Update plastic strain history for current load step
    _eps_p.set_new_values(cell_index, ip, strain_p);

    // Update equivalent plastic strain for current load step
    _eps_p_equiv.set_new_values(cell_index, ip, strain_p_eq);

    // Copy data into structures
    if (_gdim == 3)
    {
      for (std::size_t d = 0; d < 6; ++d)
        _w_stress[num_ip_per_cell*d  + ip] = trial_stress(d);

      for (std::size_t d0 = 0; d0 < 6; ++d0)
      {
        for (std::size_t d1 = 0; d1 < 6; ++d1)
        {
          const std::size_t pos = d0*6 + d1;
          _w_tangent[num_ip_per_cell*pos  + ip] = cons_tangent(d0, d1);
        }
      }
    }
    else
    {
      _w_stress[num_ip_per_cell*0  + ip] = trial_stress(0);
      _w_stress[num_ip_per_cell*1  + ip] = trial_stress(1);
      _w_stress[num_ip_per_cell*2  + ip] = trial_stress(3);

      _w_tangent[num_ip_per_cell*0  + ip] = cons_tangent(0, 0);
      _w_tangent[num_ip_per_cell*1  + ip] = cons_tangent(0, 1);
      _w_tangent[num_ip_per_cell*2  + ip] = cons_tangent(0, 3);

      _w_tangent[num_ip_per_cell*3  + ip] = cons_tangent(1, 0);
      _w_tangent[num_ip_per_cell*4  + ip] = cons_tangent(1, 1);
      _w_tangent[num_ip_per_cell*5  + ip] = cons_tangent(1, 3);

      _w_tangent[num_ip_per_cell*6  + ip] = cons_tangent(3, 0);
      _w_tangent[num_ip_per_cell*7  + ip] = cons_tangent(3, 1);
      _w_tangent[num_ip_per_cell*8  + ip] = cons_tangent(3, 3);
    }
  }
}
//-----------------------------------------------------------------------------
void ConstitutiveUpdate::update_history()
{
  // Update plastic elements
  const boost::multi_array<double, 3>& old_eps = _eps_p_equiv.old_data();
  const boost::multi_array<double, 3>& new_eps = _eps_p_equiv.current_data();

  const std::size_t num_cells = _plastic_last.shape()[0];
  const std::size_t ip_per_cell = _plastic_last.shape()[1];
  dolfin_assert(old_eps.shape()[0] == num_cells);

  for (std::size_t c = 0; c < num_cells; ++c)
  {
    for (std::size_t p = 0; p < ip_per_cell; ++p)
    {
      if ((new_eps[c][p][0] - old_eps[c][p][0] > 0.0))
        _plastic_last[c][p] = true;
      else
        _plastic_last[c][p] = false;
    }
  }

  _eps_p.update_history();
  _eps_p_equiv.update_history();
}
//-----------------------------------------------------------------------------
